package com.test.service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class UploadFileService {
	
	@Autowired
	Environment environment;
	
	@Value("${my.path}")
	private String path;
	
	public String saveFile(MultipartFile file, HttpServletRequest request) throws IOException 
	{
		
        Date date = new Date();
    	String fileName = new SimpleDateFormat("ddMMyyyyHHmmssSSS").format(date);
    	
    	String extension = FilenameUtils.getExtension(file.getOriginalFilename()).toLowerCase();//Return PNG,JPEG,JPG
    	
    	File directorio = new File(path);
    	
    	if(extension.equals("png") || extension.equals("jpeg") || extension.equals("jpg")) {
    		byte[] bytes = file.getBytes();
            
            if(!directorio.exists())
            	directorio.mkdirs(); 
            
            Path p = Paths.get(path + fileName + "." + extension);
            
            Files.write(p,bytes);

            String uri = request.getRequestURL().toString();
            
            //Devuelve la dirección que se guardará en la BD
            String newUri = uri.replace("upload", "show?name=" + fileName + ".png");
            
            return "Done: " + newUri;
    	}else {
    		  return "This file doesn't are a image";
    	}
 
    }
}
