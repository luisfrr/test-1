package com.test.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.test.exception.ResourceNotFoundException;
import com.test.model.Album;
import com.test.repository.AlbumRepository;

@Service(value = "albumService")
public class AlbumService {
	
	@Autowired
	@Qualifier("albumRepository")
	AlbumRepository albumRepository;
	
	public List<Album> getMyAlbums(long user_id){
		return albumRepository.findByUserId(user_id).orElseThrow(() -> new ResourceNotFoundException("Album", "user_id", user_id));
		
	}
	
	public String addAlbum(Album album, long user_id) {
		
		album.setDate(new Date());
		album.setUser_id(user_id);
		albumRepository.save(album);
		
		return "Done";
	}
	
	public String deleteAlbum(long album_id) {
		
		Album album = albumRepository.findById(album_id).orElseThrow(() -> new ResourceNotFoundException("Album", "album_id", album_id));
		
		albumRepository.delete(album);
		
		return "Done";
	}
	
	
	public String updateAlbum(long album_id ,String name) {
		Album album = albumRepository.findById(album_id).orElseThrow(() -> new ResourceNotFoundException("Album", "album_id", album_id));
		
		album.setName(name);
		
		albumRepository.save(album);
		
		return "Done";
	}

}
