package com.test.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.test.repository.UsersRepository;

@Service(value = "userService")
public class UsersService implements UserDetailsService {

	@Autowired
	@Qualifier("userRepository")
	UsersRepository repository;
	
	public com.test.model.User getUserId(String username) {
		return repository.findByUsername(username);
		
	}
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		com.test.model.User user = repository.findByUsername(username);
		
		return new User(user.getUsername(), user.getPassword(),
				user.isEnabled(), user.isAccountNonExpired(), 
				user.isCredentialsNonExpired(), user.isStatus(), buildgrante(user.getRol()));
	}
	
	public java.util.List<GrantedAuthority> buildgrante(byte rol){
		String[] roles = {"USER", "ADMIN"};
		
		java.util.List<GrantedAuthority> auths = new ArrayList<>();
		
		for(int i = 0; i < rol; i++) {
			auths.add(new SimpleGrantedAuthority(roles[i]));
		}
		
		return auths;
	}

}
