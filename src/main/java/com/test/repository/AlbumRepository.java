package com.test.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.test.model.Album;

@Repository(value = "albumRepository")
public interface AlbumRepository extends JpaRepository<Album, Long> {
		
	public abstract Optional<List<Album>> findByUserId(long user_id);
	
}
