package com.test.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.test.model.User;

@Repository(value = "userRepository")
public interface UsersRepository extends JpaRepository<User, Serializable> {
	
	public abstract User findByUsername(String username);
	
}
