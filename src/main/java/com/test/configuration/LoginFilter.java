package com.test.configuration;

import java.io.IOException;
import java.io.InputStream;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.test.model.User;
import com.test.service.UsersService;


public class LoginFilter extends AbstractAuthenticationProcessingFilter {
	
	@Autowired
	@Qualifier("userService")
	UsersService userService;

	protected LoginFilter(String url,AuthenticationManager authManager) {
		
		super(new AntPathRequestMatcher(url));
		setAuthenticationManager(authManager);
	}

	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
			throws AuthenticationException, IOException, ServletException {
		
		    InputStream body = request.getInputStream();

	        //UserJson user = new ObjectMapper().readValue(body, UserJson.class);
	        
	        User usuario = new ObjectMapper().readValue(body, User.class);

	        UsernamePasswordAuthenticationToken t = new UsernamePasswordAuthenticationToken(
                    usuario.getUsername(),
                    usuario.getPassword());

	        return getAuthenticationManager().authenticate(t);
	}
	
	@Override
	protected void successfulAuthentication(
			  HttpServletRequest req,
	            HttpServletResponse res, FilterChain chain,
	            Authentication auth) throws IOException, ServletException {

	        // Si la autenticacion fue exitosa, agregamos el token a la respuesta
		
		User user = userService.getUserId(auth.getName());
		
	    TokenUtil.addAuthentication(res, auth.getName(),user);
	}

}
