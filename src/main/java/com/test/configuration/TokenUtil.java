package com.test.configuration;

import static java.util.Collections.emptyList;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import com.test.model.User;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Component
public class TokenUtil {
	
	
	private static String KEY;
	private static String AUTHORIZATION;
	
	@Autowired
	public TokenUtil(@Value("${jwt.key}") String KEY,
			@Value("${jwt.authorization}") String AUTHORIZATION) {
		TokenUtil.KEY = KEY;
		TokenUtil.AUTHORIZATION = AUTHORIZATION;
	}

	
	static void addAuthentication(HttpServletResponse res, String username,User user) {
		
		Map<String, Object> claims = new HashMap<>();
		claims.put("user", user.getId());
		claims.put("sub", user.getUsername());
		
		//Genera el token
		String token = Jwts.builder().setClaims(claims)
				.signWith(SignatureAlgorithm.HS256, KEY).compact();
		
		//Agrega el token a la respuesta
		res.addHeader(AUTHORIZATION,"Bearer " + token);
	}
	
	static Authentication getAuthentication(HttpServletRequest res) {
		String token = res.getHeader(AUTHORIZATION);
		
		if(token != null) {
			String user = Jwts.parser().setSigningKey(KEY)
					.parseClaimsJws(token.replace("Bearer", ""))
					.getBody().getSubject();
			return user != null ? new UsernamePasswordAuthenticationToken(user, null,  emptyList()) : null;
		}
		return null;
	}
}
