package com.test.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class HelloController {
	
	@Value("${myProperty}")
    private String propertyValue;
	
	@GetMapping("/public/hello")
	public String helloPublic() {
		return "Welcome to REST API " + propertyValue;
	}
	
	@GetMapping("/secured/hello")
	public String helloSecured() {
		return "Welcome to REST API SECURED";
	}
	
}
