package com.test.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.test.model.Album;
import com.test.service.AlbumService;
import com.test.service.UsersService;

@RestController
@RequestMapping("api/")
public class AlbumController extends BaseController {

	public AlbumController(@Value("${jwt.key}") String KEY, @Value("${jwt.authorization}") String AUTHORIZATION) {
		super(KEY, AUTHORIZATION);
	}

	@Autowired
	@Qualifier("albumService")
	AlbumService albumService;
	
	@Autowired
	@Qualifier("userService")
	UsersService userService;
	
	
	@GetMapping("getUserIdToken/")
	public long getIdToken() {

		long id = this.getUserId();
		
		return id;
		
	}
	
	
	//GET ALL ALBUMS OF USER
	@GetMapping("getAlbums/")
	public List<Album> getUserAlbums(){
		
		return albumService.getMyAlbums(this.getUserId());
	}
	
	
	//ADD ALBUM
	@PostMapping("albums/")
	public ResponseEntity<?> addAlbum(@Valid @RequestBody() Album album) {

		String resultado = albumService.addAlbum(album,this.getUserId());
		
		return new ResponseEntity<Object>(resultado, HttpStatus.OK);
	}
	
	
	//DELETE ALBUM
	@DeleteMapping("albums/{album_id}")
	public ResponseEntity<?> deleteAlbum(@PathVariable("album_id") long album_id) {		
		String resultado = albumService.deleteAlbum(album_id);
		
		if(resultado != "Done")
			return new ResponseEntity<Object>(resultado, HttpStatus.NOT_FOUND);
		else
			return new ResponseEntity<Object>(resultado, HttpStatus.OK);
	}
	
	//UPDATE ALBUM
	@PutMapping("albums/{album_id}/{new_name}")
	public ResponseEntity<?> updateAlbum(@PathVariable("new_name") String name,
			@PathVariable("album_id") long album_id){
		
		
		String resultado;
		resultado =  albumService.updateAlbum(album_id, name);
		
		return new ResponseEntity<Object>(resultado, HttpStatus.OK);
	}
}
