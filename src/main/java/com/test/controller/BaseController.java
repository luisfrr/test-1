package com.test.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;


public abstract class BaseController {
	
	private static String KEY;
    private static String AUTHORIZATION;
	
	public BaseController(@Value("${jwt.key}") String KEY, 
			@Value("${jwt.authorization}") String AUTHORIZATION) {
		BaseController.KEY = KEY;
		BaseController.AUTHORIZATION = AUTHORIZATION;
	}

	public long getUserId() {
		RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
		HttpServletRequest res = ((ServletRequestAttributes) requestAttributes).getRequest();
		
		//GET token de la cabecera
	    String token = res.getHeader(AUTHORIZATION);
				
		//GET Claims del token
		Claims user = Jwts.parser().setSigningKey(KEY)
				.parseClaimsJws(token.replace("Bearer", "")).getBody();
				
		//GET user_id de Claims del token
		Integer id = (Integer) user.get("user");
		
		return id.longValue();
	}
}
