package com.test.controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.test.service.UploadFileService;


@RestController
@RequestMapping("api/")
public class ImageDontProcessedController {

	@Autowired
    private UploadFileService uploadFileService;
	
	@Value("${my.path}")
	private String myPath;

    @GetMapping("image/")
    public String index(){
        return "api/image/";
    }

    @RequestMapping(value = "image/show", method = RequestMethod.GET ,produces = {MediaType.IMAGE_PNG_VALUE})
    public void getImage(HttpServletResponse response, @RequestParam("name") String nombre)
    		throws IOException {
    	
    	Path path = Paths.get(myPath + nombre);
    	
    	Logger.getLogger(getClass().getName()).log(
	            Level.INFO, path.toString());
    	
    	byte[] data = Files.readAllBytes(path);
    	
    	response.setContentType(MediaType.IMAGE_PNG_VALUE);
        
    	org.springframework.util.StreamUtils.copy(data, response.getOutputStream());
    }
	
    @PostMapping("image/upload")
    public ResponseEntity<?> uploadFile(@Value("file") @RequestBody() @Nullable MultipartFile file,
    		HttpServletRequest request){
    	
        if (file.isEmpty()) {
        	return new ResponseEntity<Object>("Choose file", HttpStatus.OK);
        }
            
        String resultado = "";
        
        try 
        {
            resultado = uploadFileService.saveFile(file,request);
        } 
        catch (IOException e) 
        {
            e.printStackTrace();
        }

        return new ResponseEntity<Object>(resultado, HttpStatus.OK);
    }
    
    @PostMapping("image/multipleUpload")
    public ResponseEntity<?> multipleUpload(HttpServletRequest request,@RequestParam("file") MultipartFile... files){
    	    
        String resultado = "";
        
        try 
        {
            for(MultipartFile file : files) {
            	resultado = uploadFileService.saveFile(file,request);
            }
        } 
        catch (Exception e) 
        {
            e.printStackTrace();
        }

        return new ResponseEntity<Object>(resultado, HttpStatus.OK);
    }
}
