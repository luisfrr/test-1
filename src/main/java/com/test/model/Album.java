package com.test.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "album")
public class Album implements Serializable {

	private static final long serialVersionUID = -8544071734133028812L;
	
	//ATT
	
	@Id
	@Column(name = "album_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long album_id;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "date")
	@JsonFormat(pattern="dd/MM/yyyy")
	private Date date;
	
	@Column(name = "user_id", nullable = false)
	private long userId;
	
	//Constructor
	
	public Album() {
		
	}
	
	public Album(String name,Date date, long user_id) {
		this.name = name;
		this.date = date;
		this.userId = user_id;
	}
	
	//GETTERS & SETTERS
	
	public long getAlbum_id() {
		return album_id;
	}

	public void setAlbum_id(long album_id) {
		this.album_id = album_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getUser_id() {
		return userId;
	}

	public void setUser_id(long user_id) {
		this.userId = user_id;
	}
	
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	
}
